import dispatcher from 'dispatcher';
import utils from 'utils';


var items = {}
var categoryItems = {
    containerDesktop: null,
    containerMobile: null,
    values: {}
}
var idName = 'new-id-';
var idNum = 1;
var historyCount = 0;

var _handleHistory = function(response, order, p) {
    let string;
    let state = {};
    let count = 0;

    if (response) {
        for (let key in response) {
            if (key === 'categories') {
                let type = response[key].key;
                let name = response[key].selected[0].id;
                let link = response[key].selected[0].link;

                state[key] = {
                    link: link
                }
                string = window.location.origin + link;

                state[key] = {
                    name: name,
                    type: type,
                    link: link
                }
            }
            if (key === 'filters') {
                let filters = response[key];

                for (let key1 in filters) {
                    if (!filters[key1].multiply && filters[key1].selected) {
                        let type = filters[key1].key;
                        let name = filters[key1].selected[0].text;

                        if (count === 0) {
                            string += `?${type}=${name}`;
                            count++
                        } else {
                            string += `&${type}=${name}`;
                            count++
                        }
                        state[key1] = {
                            name: name,
                            type: type
                        }
                    } else if (filters[key1].multiply && filters[key1].selected) {
                        let type = filters[key1].key;
                        let name = [];
                        let smartFilters = filters[key1].selected;

                        for (let i = 0; i < smartFilters.length; i++) {
                            name.push(smartFilters[i].text);

                            if (count === 0) {
                                string += `?${type}=${name}`;
                                count++
                            } else {
                                string += `&${type}=${name}`;
                                count++
                            }
                        }
                        state[key1] = {
                            name: name,
                            type: type
                        }
                    }
                }
            }
        }
    }

    if (order) {
        let type = 'order';
        let name = order;

        if (count === 0) {
            string += `?${type}=${name}`
            count++
        } else {
            string += `&${type}=${name}`
            count++
        }
        state[type] = {
            name: name,
            type: type
        }
    }

    if (p) {
        let type = 'p';
        let name = p;

        if (count === 0) {
            string += `?${type}=${name}`
            count++
        } else {
            string += `&${type}=${name}`
            count++
        }
        state[type] = {
            name: name,
            type: type
        }
    }

    if (count > 0) {
        historyCount++
        history.pushState(state, '', string);
    }
}

var _handleChange = function (response) {
    function _updateCategories() {
        // Взяли из ответа блок с категориями
        var categories = response.categories.values;
        var name = response.categories.key;

        // Удаляем старые категории из DOM
        for (var key in categoryItems.values) {
            categoryItems.values[key].element.remove();
            categoryItems.values[key].mobileElement.remove();
        }

        // Обновляем имя контейнера для категорий
        var containerDesktop = categoryItems.containerDesktop;
        var containerMobile = categoryItems.containerMobile;
        containerDesktop.setAttribute('data-name', name);
        containerMobile.setAttribute('data-name', name);

        // Создаем новые DOM-элементы с категориями и добавляем в контейнер
        for (let i = 0; i < categories.length; i++) {
            _updateDesktop(categories[i], containerDesktop);
            _updateMobile(categories[i], containerMobile);
        }

        function _updateDesktop(category, container) {
            var desktopCategory = document.createElement('div');
            desktopCategory.classList.value = 'item filter-category filter-control';
            if (category.selected) {
                desktopCategory.classList.add('active');
            }
            desktopCategory.setAttribute('data-id', category.id);
            desktopCategory.innerHTML =
                `<div class="num">${category.count}</div>` +
                `<div class="text">${category.name}</div>`;
            container.appendChild(desktopCategory);
        }

        function _updateMobile(category, container) {
            var mobileCategory = document.createElement('div');
            var current = container.parentNode.parentNode.querySelector('.current');
            mobileCategory.classList.value = 'it filter-category filter-control mobile-control';
            if (category.selected) {
                mobileCategory.classList.add('active');
                current.innerHTML = category.name;
            }
            mobileCategory.setAttribute('data-id', category.id);
            mobileCategory.innerHTML = category.name;
            container.appendChild(mobileCategory);
        }
    }
    function _updateFilters() {
        // Взяли из ответа блок с фильтрами
        var filters = response.filters;

        // Удаляем старые фильтры из DOM
        for (var name in items) {
            for (var id in items[name].values) {
                items[name].values[id].element.remove();
                items[name].values[id].mobileElement.remove();
            }
        }

        // Создаем новые DOM-элементы с фильтрами и добавляем в контейнер
        for (var key in filters) {
            // Получаем параметры фильтра
            var filter = filters[key];
            var name = filter.key;
            var multiply = filter.multiply;
            var values = filter.values;

            var hasActive = false;

            if (!values) continue;

            // Обновляем контейнер конкретного фильтра
            var containerDesktop = items[name].containerDesktop;
            containerDesktop.setAttribute('data-name', name);
            var containerMobile = items[name].containerMobile;
            containerMobile.setAttribute('data-name', name);

            // Создаем контролы фильтра и добавляем в контейнер
            for (let i = 0; i < values.length; i++) {
                _updateDesktop(values[i], containerDesktop);
                _updateMobile(values[i], containerMobile);
            }
        }

        function _updateDesktop(value, container) {
            var current = container.parentNode.parentNode.querySelector('.current');
            var control = document.createElement('div');
            control.classList.value = 'it filter-properties filter-control';

            if (multiply) {
                control.setAttribute('multiply', '');
            }

            control.setAttribute('data-id', value.text);
            control.innerHTML = value.text;
            if (value.selected) {
                control.classList.add('active');
                current.innerHTML = value.text
                hasActive = true;
                container.parentNode.parentNode.classList.add('has-active');
            }
            if (!hasActive) {
                current.innerHTML = 'Не выбран';
                container.parentNode.parentNode.classList.remove('has-active');
            }
            container.appendChild(control);
        }

        function _updateMobile(value, container) {
            var current = container.parentNode.parentNode.querySelector('.current');
            var control = document.createElement('div');
            control.classList.value = 'it filter-properties filter-control mobile-control';

            if (multiply) {
                control.setAttribute('multiply', '');
            }

            control.setAttribute('data-id', value.text);
            control.innerHTML = value.text;
            if (value.selected) {
                control.classList.add('active');
                current.innerHTML = value.text
                hasActive = true;
                container.parentNode.parentNode.classList.add('has-active');
            }
            if (!hasActive) {
                current.innerHTML = 'Не выбран';       
                container.parentNode.parentNode.classList.remove('has-active');
            }
            container.appendChild(control);
        }
    }

    _updateCategories();
    _updateFilters();

    var hasActiveDesktop = document.querySelector('.filter .has-active');
    var hasActiveMobile = document.querySelector('.filter-popup .item.has-active');
    if (hasActiveDesktop) {
        var filter = document.querySelector('.filter');
        var filterPopup = document.querySelector('.popup.filter-popup');
        filter.classList.add('has-active');
        filterPopup.classList.add('has-active');
    } else {
        var filter = document.querySelector('.filter');
        var filterPopup = document.querySelector('.popup.filter-popup');
        filter.classList.remove('has-active');
        filterPopup.classList.remove('has-active');
    }

    setTimeout(function () {
        dispatcher.dispatch({
            type: 'filter-mutate'
        });
    }, 300)
}

var _add = function (items, element) {
    var id = element.getAttribute('data-id');
    var mobileElement = document.querySelector(`.filter-control.mobile-control[data-id="${id}"]`);

    var containerDesktop = element.parentNode;
    var name = containerDesktop.getAttribute('data-name');
    var containerMobile = mobileElement.parentNode;

    var isCategory = element.classList.contains('filter-category');
    if (isCategory) {
        if (!categoryItems.containerDesktop && !categoryItems.containerMobile) {
            categoryItems.containerDesktop = containerDesktop;
            categoryItems.containerMobile = containerMobile;
        }
        categoryItems.values[id] = {
            id: id,
            element: element,
            mobileElement: mobileElement,
            containerDesktop: containerDesktop,
            containerMobile: containerMobile,
            name: name
        }
    } else {
        if (items[name] == null) {
            items[name] = {
                containerDesktop: null,
                containerMobile: null,
                values: {}
            }
        }
        items[name].containerDesktop = containerDesktop;
        items[name].containerMobile = containerMobile;
        items[name].values[id] = {
            id: id,
            element: element,
            mobileElement: mobileElement,
            containerDesktop: containerDesktop,
            containerMobile: containerMobile,
            name: name
        }
    }
}

var _remove = function (items, item) {
    delete items[item.id];
}

var _handleMutate = function () {
    var elements;

    var check = function (items, element) {
        var found = false;
        for (var id in items) {
            if (items.hasOwnProperty(id)) {
                if (items[id].element === element) {
                    found = true;
                    break;
                }
            }
        }
        if (!found) {
            _add(items, element);
        }
    }

    var backCheck = function (items, elements, item) {
        var element = item.element;
        var found = false;
        for (var i = 0; i < elements.length; i++) {
            if (elements[i] === item.element) {
                found = true;
                break;
            }
        }
        if (!found) {
            _remove(items, item);
        }
    }

    var topFilters = document.querySelectorAll('.top-filter .filter-control');
    var botFilters = document.querySelectorAll('.bot-filter .filter-control');

    topFilters = Array.prototype.slice.call(topFilters);
    botFilters = Array.prototype.slice.call(botFilters);

    elements = topFilters.concat(botFilters);


    for (var i = 0; i < elements.length; i++) {
        check(items, elements[i]);
    }
    for (var id in items) {
        if (items.hasOwnProperty(id)) {
            backCheck(items, elements, items[id]);
        }
    }
}

var init = function () {
    _handleMutate();

    dispatcher.subscribe(function (e) {
        if (e.type === 'filter-load') {
            _handleChange(e.response);
        }
        if (e.type === 'mutate' || e.type === 'filter-mutate' || e.type === 'pagination-mutate') {
            _handleMutate();
        }
        if (e.type === 'history-update') {
            _handleHistory(e.response, e.order, e.p);
        }
    });
}

export default {
    init: init
}