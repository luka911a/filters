import dispatcher from 'dispatcher';
import utils from 'utils';
import axios from 'axios';

var items = {};
var idName = 'new-id-';
var idNum = 1;
var actionFilters;
var actionPaginations;

function _handleStart() {
    let data = new FormData();
    let newQuery = utils.queryParse(location.search);

    if (Object.keys(newQuery).length == 0) {
        for (let key in items) {
            if (items[key].active) {
                data.append(items[key].name, key);
            }
        }
        data.append('sort', document.querySelector('.sort .it.active').getAttribute('data-id'));
        data.append('p', 1);
    } else {
        for (let key in items) {
            if (items[key].active) {
                data.append(items[key].name, key);
            }
        }
        for (let key in newQuery) {
            data.append(key, newQuery[key]);
        }
    }

    _sendRequest(data, false, false);
}

function _sendRequest(data, popState, pagination) {
    axios.all([_actionFilters(data), _actionPaginations(data)])
        .then(axios.spread(function (filtersResponse, paginationsResponse) {
            if (filtersResponse.status !== 200 && paginationsResponse.status !== 200) {
                console.log('Это фиаско братан');
            } else if (filtersResponse.status === 200 && paginationsResponse.status === 200) {
                dispatcher.dispatch({
                    type: 'filter-load',
                    response: filtersResponse.data
                });
                if (!pagination) {
                    dispatcher.dispatch({
                        type: 'pagination-load',
                        p: paginationsResponse.data.p,
                        paginationSize: paginationsResponse.data['total'] / paginationsResponse.data['limit']
                    });
                }
                dispatcher.dispatch({
                    type: 'catalog-load',
                    response: paginationsResponse.data['response-catalog']
                });
                if (!popState) {
                    dispatcher.dispatch({
                        type: 'history-update',
                        response: filtersResponse.data,
                        order: paginationsResponse.data.sort,
                        p: paginationsResponse.data.p
                    });
                }   
            }
        }))
        .catch(function (error) {
            console.warn(error);
        });
}

function _actionFilters(data) {
    return axios.post(actionFilters, data)
}

function _actionPaginations(data) {
    return axios.post(actionPaginations, data)
}

function _handlePopState() {
    var data = new FormData();
    var state = history.state;

    for (let key in state) {
        if (key === 'categories') {
            data.append(state[key].type, state[key].name);
        } else if (key !== 'order' && key !== 'p') {
            let nameArr = state[key].name;
            if (Array.isArray(nameArr)) {
                for (let i = 0; i < nameArr.length; i++) {
                    data.append(state[key].type, state[key].name[i]);
                }
            } else {
                data.append(state[key].type, state[key].name);
            }
        } else if (key === 'order') {
            data.append('sort', state[key].name);
        } else if (key === 'p') {
            data.append('p', state[key].name);
        }
    }
    _sendRequest(data, true, false);
}

function _toggleCover(id) {
    if (!items.hasOwnProperty(id)) return;
    var cover = items[id].mobileElement.parentNode.parentNode.parentNode;
    if (!cover || !cover.classList.contains('item-cover')) return;
    if (cover.querySelector('.checkbox-container')) return;
    cover.classList.remove('active');
}

function _handleClick(id) {
    if (!items[id].multiply) {
        items[id].element.classList.toggle('active');
        items[id].mobileElement.classList.toggle('active');
        items[id].active = !items[id].active;

        for (var key in items) {
            if (key === id) continue;
            if (items[key].name === items[id].name) {
                items[key].element.classList.remove('active');
                items[key].mobileElement.classList.remove('active');
                items[key].active = false;
            }
        }
    } else {
        items[id].element.classList.toggle('active');
        items[id].mobileElement.classList.toggle('active');
        items[id].active = !items[id].active;
    }

    let data = new FormData();

    for (let key in items) {
        if (items[key].active) {
            data.append(items[key].name, key);
        }
    }
    data.append('sort', document.querySelector('.sort .it.active').getAttribute('data-id'));
    let newQuery = utils.queryParse(location.search);
    for (let key in newQuery) {
        if (key === 'p') {
            data.append(key, newQuery[key]);
        }
    }

    _toggleCover(id);
    _sendRequest(data, false, false);
}

function _filterClear(name) {
    for (var key in items) {
        if (items[key].name === name) {
            items[key].active = false;
            items[key].element.classList.remove('active');
            items[key].mobileElement.classList.remove('active');
        }
    }

    let data = new FormData();

    for (let key in items) {
        if (items[key].active) {
            data.append(items[key].name, key);
        }
    }
    data.append('sort', document.querySelector('.sort .it.active').getAttribute('data-id'));
    let newQuery = utils.queryParse(location.search);
    for (let key in newQuery) {
        if (key === 'p') {
            data.append(key, newQuery[key]);
        }
    }

    _sendRequest(data, false, false);
}

function _filterClearAll() {
    for (var key in items) {
        items[key].active = false;
        items[key].element.classList.remove('active');
        items[key].mobileElement.classList.remove('active');
    }
    
    var firstCategory = document.querySelector('.top-filter .filter-category');
    var id = firstCategory.getAttribute('data-id');
    items[id].active = true;
    items[id].element.classList.add('active');
    items[id].mobileElement.classList.add('active');

    let data = new FormData();

    for (let key in items) {
        if (items[key].active) {
            data.append(items[key].name, key);
        }
    }
    data.append('sort', document.querySelector('.sort .it.active').getAttribute('data-id'));
    let newQuery = utils.queryParse(location.search);
    for (let key in newQuery) {
        if (key === 'p') {
            data.append(key, newQuery[key]);
        }
    }

    _sendRequest(data, false, false);
}

var _add = function (items, element) {
    var id = element.getAttribute('data-id');
    var name = element.parentNode.getAttribute('data-name');
    var active = element.classList.contains('active');
    var multiply = false;
    if (element.hasAttribute('multiply')) {
        multiply = true;
    }
    var mobileElement = document.querySelector(`.mobile-control[data-id="${id}"]`);

    if (!id) {
        id = idName + idNum;
        idNum++;
        element.setAttribute('data-id', id);
    }

    element.addEventListener('click', _handleClick.bind(null, id));
    mobileElement.addEventListener('click', _handleClick.bind(null, id));
    
    items[id] = {
        id: id,
        name: name,
        element: element,
        mobileElement: mobileElement,
        active: active,
        multiply: multiply
    }
}

var _remove = function (items, item) {
    delete items[item.id];
}

var _handleMutate = function () {
    var elements;

    if (document.querySelector('.catalog-page')) {
        actionFilters = document.querySelector('.filter').getAttribute('data-action');
        actionPaginations = document.querySelector('.pagination').getAttribute('data-action');
        
        window.onpopstate = function(event) {
            _handlePopState();
        };
    }

    var check = function (items, element) {
        var found = false;
        for (var id in items) {
            if (items.hasOwnProperty(id)) {
                if (items[id].element === element) {
                    found = true;
                    break;
                }
            }
        }
        if (!found) {
            _add(items, element);
        }
    }

    var backCheck = function (items, elements, item) {
        var element = item.element;
        var found = false;

        for (var i = 0; i < elements.length; i++) {
            if (elements[i] === item.element) {
                found = true;
                break;
            }
        }

        if (!found) {
            _remove(items, item);
        }
    }

    var topFilters = document.querySelectorAll('.top-filter .filter-control');
    var botFilters = document.querySelectorAll('.bot-filter .filter-control');

    topFilters = Array.prototype.slice.call(topFilters);
    botFilters = Array.prototype.slice.call(botFilters);

    elements = topFilters.concat(botFilters);
    for (var i = 0; i < elements.length; i++) {
        check(items, elements[i]);
    }
    for (var id in items) {
        if (items.hasOwnProperty(id)) {
            backCheck(items, elements, items[id]);
        }
    }
}

var init = function () {
    if (!document.querySelector('.catalog-page')) return

    _handleMutate();
    _handleStart();

    dispatcher.subscribe(function (e) {
        if (e.type === 'mutate' || e.type === "filter-mutate") {
            _handleMutate();
        }
        if (e.type === 'sort-change') {
            let data = new FormData();

            for (let key in items) {
                if (items[key].active) {
                    data.append(items[key].name, key);
                }
            }

            let newQuery = utils.queryParse(location.search);
            if (e.sort) {
                data.append('sort', e.sort);
            } else {
                for (let key in newQuery) {
                    if (key === 'order') {
                        data.append('sort', newQuery[key]);
                    }
                }
            }
            if (e.p) {
                data.append('p', e.p);
            } else {
                for (let key in newQuery) {
                    if (key === 'p') {
                        data.append('p', newQuery[key]);
                    }
                }
            }

            _sendRequest(data, false, true);
        }
        if (e.type === 'filter-clear') {
            _filterClear(e.name);
        }
        if (e.type === 'filter-clear-all') {
            _filterClearAll();
        }
    });
}

export default {
    init: init
}
